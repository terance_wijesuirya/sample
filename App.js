/**
 * @author Terance
 */
import React, {Component} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {NavigationContainer} from '@react-navigation/native';
import {createDrawerNavigator} from '@react-navigation/drawer';

import Splash from './src/screens/Splash';
import Login from './src/screens/Login';
import ItemList from './src/screens/ItemList';
import SongList from './src/screens/SongList';
import PlayList from './src/screens/PlayList';

import {DrawerLayoutContent} from './src/screens/DrawerLayoutContent';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerMain() {
  const Drawer = createDrawerNavigator();
  return (
    <Drawer.Navigator
      initialRouteName="ItemList"
      drawerContent={(props) => <DrawerLayoutContent {...props} />}>
      <Drawer.Screen name="ItemList" component={ItemList} />
      <Drawer.Screen
        name="SongList"
        component={SongList}
        options={{headerShown: false}}
      />
    </Drawer.Navigator>
  );
}

export default class App extends Component {
  render() {
    return (
      <NavigationContainer independent={true}>
        <Stack.Navigator
          initialRouteName="Splash"
          component={Splash}
          screenOptions={{headerShown: false}}>
          <Stack.Screen
            name="Splash"
            component={Splash}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="Login"
            component={Login}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="ItemList"
            component={DrawerMain}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="SongList"
            component={SongList}
            options={{headerShown: false}}
          />
          <Stack.Screen
            name="PlayList"
            component={PlayList}
            options={{headerShown: false}}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
