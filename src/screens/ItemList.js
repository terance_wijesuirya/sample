import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  SafeAreaView,
  FlatList,
} from 'react-native';
import MainHeader from '../component/MainHeader';

export default class ItemList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      refreshing: true,
    };
  }

  componentDidMount() {
    this.fetchDetails();
  }

  fetchDetails() {
    this.setState({refreshing: true});
    fetch('https://api.thecatapi.com/v1/images/search?limit=10&page=1')
      .then((res) => res.json())
      .then((resJson) => {
        this.setState({data: resJson});
        this.setState({refreshing: false});
      })
      .catch((e) => console.log(e));
  }

  renderItemComponent = (data) => (
    <TouchableOpacity style={styles.container}>
      <Image style={styles.image} source={{uri: data.item.url}} />
    </TouchableOpacity>
  );

  ItemSeparator = () => (
    <View
      style={{
        height: 2,
        backgroundColor: 'rgba(0,0,0,0.5)',
        marginLeft: 10,
        marginRight: 10,
      }}
    />
  );

  handleRefresh = () => {
    this.setState({refreshing: false}, () => {
      this.fetchDetails();
    });
  };

  render() {
    return (
      <SafeAreaView>
        <MainHeader title={'Iitem List'} />
        <FlatList
          data={this.state.data}
          renderItem={(item) => this.renderItemComponent(item)}
          keyExtractor={(item) => item.id.toString()}
          ItemSeparatorComponent={this.ItemSeparator}
          refreshing={this.state.refreshing}
          onRefresh={this.handleRefresh}
        />
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    height: 200,
    margin: 10,
    backgroundColor: '#FFF',
    borderRadius: 6,
  },
  image: {
    height: '80%',
    borderRadius: 4,
  },
});
