import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  PermissionsAndroid,
  DeviceEventEmitter,
  TouchableOpacity,
  ScrollView,
  Image,
} from 'react-native';
import {RNAndroidAudioStore} from 'react-native-get-music-files';
import Header from '../component/Header';
import ComponentStyles from '../../constant/Component.styles';

let audioList1 = [];

export default class SongList extends Component {
  constructor(props) {
    super(props);

    this.requestPermission = async () => {
      try {
        const granted = await PermissionsAndroid.requestMultiple(
          [
            PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
            PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          ],
          {
            title: 'Permission',
            message: 'Storage access is requiered',
            buttonPositive: 'OK',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          alert('You can use the package');
        } else {
          this.requestPermission();
        }
      } catch (err) {
        console.warn(err);
      }
    };

    this.getAll = () => {
      RNAndroidAudioStore.getAll({
        blured: true, // works only when 'cover' is set to true
        artist: true,
        duration: true, //default : true
        cover: true, //default : true,
        genre: true,
        title: true,
        cover: true,
        minimumSongDuration: 10000, // get songs bigger than 10000 miliseconds duration,
        batchNumber: 1,
        delay: 1000,
      }).catch((er) => alert(JSON.stringify(error)));

      if (this.state.tracks.length === 0) {
        console.log('ZERO');
      } else {
        console.log('START');
        var count = 0;
        for (var i = 0; i < this.state.tracks.length; i++) {
          let temp = this.state.tracks[i];
          for (let j = 0; j < temp.length; j++) {
            count = count + 1;

            var tim = Number(temp[j].duration);
            var numString = count.toString();
            var prefix = 'file://';
            var rest = temp[j].path;
            var path = prefix.concat(rest);
            var duration = Math.round(tim / 1000);

            audioList1.push({
              id: numString,
              url: path,
              title: temp[j].title,
              artist: temp[j].author,
              artwork:
                'https://scene360.com/wp-content/uploads/2014/10/computergraphics-album-covers-2014-03.jpg',
              duration: duration,
            });
          }
        }
        console.log('END');
      }
    };

    this.state = {
      getAlbumsInput: '',
      getSongsInput: {},
      searchParam: '',
      tracks: [],
      artists: [],
      albums: [],
      songs: [],
      search: [],
    };
  }

  async componentDidMount() {
    this.requestPermission();

    DeviceEventEmitter.addListener('onBatchReceived', (params) => {
      this.setState({
        ...this.state,
        tracks: [...this.state.tracks, params.batch],
      });
    });

    DeviceEventEmitter.addListener('onLastBatchReceived', (params) => {
      this.setState(alert('last batch sent'));
    });

    this.props.navigation.addListener('focus', () => {});
  }

  componentWillUnmount() {
    DeviceEventEmitter.removeAllListeners();
  }

  goToPlayer(value) {
    this.props.navigation.navigate('PlayList', {
      songList: audioList1,
    });
  }

  render() {
    return (
      <View style={styles.container}>
        <Header
          title={'Song List'}
          navigation={() => this.props.navigation.goBack()}
        />
        {/* <TouchableOpacity style={styles.button} onPress={() => this.getAll()}>
          <Text style={styles.buttonText}>Load</Text>
        </TouchableOpacity> */}
        <TouchableOpacity
          style={styles.button}
          onPress={() => this.goToPlayer(this.state.tracks)}>
          <Text style={styles.buttonText}>LOGIN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  button: {
    width: '90%',
    backgroundColor: '#00aeef',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: ComponentStyles.WIDTH * 0.05,
  },
  gridView: {
    marginTop: 10,
    flex: 1,
  },
  itemContainer: {
    justifyContent: 'flex-end',
    borderRadius: 5,
    padding: 10,
    height: 150,
    backgroundColor: '#00aeef',
  },
  itemName: {
    fontSize: 16,
    color: '#fff',
    fontWeight: '600',
  },
  itemIocn: {
    width: ComponentStyles.WIDTH * 0.2,
    height: ComponentStyles.HEIGHT * 0.2,
  },
  buttonShuffle: {
    width: '90%',
    backgroundColor: '#00aeef',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
});
