import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import ComponentStyles from '../../constant/Component.styles';
import {LG} from '../images/index';

export default class Login extends Component {
  state = {
    email: '',
    password: '',
  };

  gotoItemList() {
    this.props.navigation.replace('ItemList');
  }

  render() {
    return (
      <View
        style={[
          ComponentStyles.CONTAINER,
          {justifyContent: 'center', alignItems: 'center'},
        ]}>
        <Image source={LG} style={styles.logo} resizeMode="contain" />
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Email..."
            placeholderTextColor="black"
            onChangeText={(text) => this.setState({email: text})}
          />
        </View>
        <View style={styles.inputView}>
          <TextInput
            style={styles.inputText}
            placeholder="Password..."
            placeholderTextColor="black"
            onChangeText={(text) => this.setState({password: text})}
          />
        </View>
        <TouchableOpacity>
          <Text style={styles.forgot}>Forgot Password?</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={() => this.gotoItemList()}>
          <Text style={styles.loginText}>LOGIN</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Text style={styles.signUpText}>Signup</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    width: ComponentStyles.WIDTH * 0.2,
    height: ComponentStyles.HEIGHT * 0.2,
  },
  inputView: {
    width: '80%',
    backgroundColor: 'white',
    borderRadius: 30,
    height: 50,
    marginBottom: 20,
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    padding: 20,
  },
  inputText: {
    height: 50,
    color: 'black',
    fontSize: ComponentStyles.WIDTH * 0.05,
  },
  forgot: {
    color: 'red',
    fontSize: ComponentStyles.WIDTH * 0.04,
  },
  loginBtn: {
    width: '80%',
    backgroundColor: '#00aeef',
    borderRadius: 25,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    marginBottom: 10,
  },
  loginText: {
    color: 'white',
    fontSize: ComponentStyles.WIDTH * 0.05,
  },
  signUpText: {
    color: '#00aeef',
    fontSize: ComponentStyles.WIDTH * 0.05,
  },
});
