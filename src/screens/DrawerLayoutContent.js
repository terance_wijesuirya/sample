import React from 'react';
import {View, StyleSheet, ImageBackground, Image, Text} from 'react-native';
import {Avatar, Title, Caption, Drawer} from 'react-native-paper';
import {DrawerContentScrollView, DrawerItem} from '@react-navigation/drawer';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconA from 'react-native-vector-icons/AntDesign';

import ComponentStyles from '../../constant/Component.styles';

export function DrawerLayoutContent(props) {
  return (
    <View style={{flex: 1, backgroundColor: 'white', opacity: 1}}>
      <DrawerContentScrollView {...props}>
        <View style={{margin: 0, flex: 1, height: '100%'}}>
          <View style={{flexDirection: 'column', alignContent: 'center'}}>
            <ImageBackground
              source={require('../images/navigation_header.png')}
              style={{
                width: '100%',
                alignItems: 'center',
                paddingTop: 50,
                paddingBottom: 30,
                flexDirection: 'column',
              }}
              resizeMode="stretch">
              <Avatar.Image
                source={require('../images/image1.jpg')}
                size={70}
              />
              <View style={{flexDirection: 'column', alignItems: 'center'}}>
                <Title style={styles.title}>Mr Kasun Perera</Title>
                <Caption style={styles.caption}>+94 77354627873</Caption>
              </View>
            </ImageBackground>
          </View>

          <Drawer.Section style={styles.drawerSection}>
            <DrawerItem
              style={styles.DrawerItemStyle}
              icon={({color, size}) => (
                <Icon
                  name="home-circle-outline"
                  color={ComponentStyles.COLORS.BLACK}
                  size={size}
                />
              )}
              label={() => <Text style={{color: 'black'}}>Home</Text>}
              onPress={() => {
                props.navigation.navigate('ItemList');
              }}
            />
            <DrawerItem
              style={styles.DrawerItemStyle}
              icon={({color, size}) => (
                <IconA
                  name="user"
                  color={ComponentStyles.COLORS.BLACK}
                  size={size}
                />
              )}
              label={() => <Text style={{color: 'black'}}>Profile</Text>}
              onPress={() => {
                props.navigation.navigate('SongList');
              }}
            />
            <DrawerItem
              style={styles.DrawerItemStyle}
              icon={({color, size}) => (
                <IconA
                  name="logout"
                  color={ComponentStyles.COLORS.BLACK}
                  size={size}
                />
              )}
              label={() => <Text style={{color: 'black'}}>Logout</Text>}
              onPress={async () => {
                props.navigation.replace('Login');
              }}
            />
          </Drawer.Section>
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  drawerContent: {
    flex: 1,
  },
  userInfoSection: {
    paddingLeft: 0,
  },
  title: {
    fontSize: 16,
    marginTop: 3,
    fontWeight: 'bold',
    color: 'white',
  },
  caption: {
    fontSize: 14,
    lineHeight: 14,
    color: 'white',
  },

  drawerSection: {
    marginTop: 10,
    height: '100%',
  },

  DrawerItemStyle: {
    height: 45,
    paddingLeft: 0,
    fontFamily: ComponentStyles.FONT_FAMILY.REGULAR,
    tintColor: '#ffffff',
    fontSize: ComponentStyles.HEIGHT * 0.2,
  },
});
