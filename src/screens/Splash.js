import React, {Component} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import ComponentStyles from '../../constant/Component.styles';
import {BG, LG, BRAND} from '../images/index';

export default class Splash extends Component {
  async componentDidMount() {
    setTimeout(() => {
      this.props.navigation.replace('Login');
    }, 2000);
  }

  render() {
    return (
      <View
        style={[
          ComponentStyles.CONTAINER,
          {justifyContent: 'center', alignItems: 'center'},
        ]}>
        <Image source={BG} style={styles.bg} resizeMode="stretch" />
        <Image source={LG} style={styles.logo} resizeMode="contain" />
        <Image source={BRAND} style={styles.logoText} resizeMode="contain" />
        <Text style={styles.text}>{'Powered By Zincat'}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  bg: {
    position: 'absolute',
    width: ComponentStyles.WIDTH,
    height: ComponentStyles.HEIGHT,
  },
  logo: {
    width: ComponentStyles.WIDTH * 0.4,
    height: ComponentStyles.HEIGHT * 0.4,
  },
  logoText: {
    width: ComponentStyles.WIDTH * 0.5,
    height: ComponentStyles.HEIGHT * 0.2,
  },
  text: {
    position: 'absolute',
    fontSize: ComponentStyles.WIDTH * 0.05,
    fontFamily: ComponentStyles.FONT_FAMILY.REGULAR,
    bottom: 30,
    alignSelf: 'center',
    color: ComponentStyles.COLORS.LIGHT_BLUE,
  },
});
