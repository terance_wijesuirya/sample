/**
 * @author Shanilka
 */
import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import comStyles from '../../constant/Component.styles';

export default class MainHeader extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>{this.props.title}</Text>
        <View style={{flex: 1}} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    padding: 15,
    height: 60,
    backgroundColor: comStyles.COLORS.BLUE,
    alignItems: 'center',
    justifyContent: 'center',
    ...Platform.select({
      ios: {
        shadowColor: '#A5A5A5',
        shadowOffset: {height: 4, width: 0},
        shadowOpacity: 0.7,
        shadowRadius: 5,
      },
      android: {
        shadowOpacity: 0.2,
        elevation: 10,
        shadowColor: '#F3F3F3',
      },
    }),
  },
  title: {
    position: 'absolute',
    color: comStyles.COLORS.WHITE,
    fontFamily: comStyles.FONT_FAMILY.BOLD,
    fontSize: 24,
    paddingLeft: 5,
    top: 2,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  spacing: {
    paddingLeft: 17,
  },
});
